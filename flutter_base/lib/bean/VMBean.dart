import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sp_util/sp_util.dart';

///模板
///
/// 全局初始化使用:  await SpUtil.getInstance();
// void main() {
//   runZoned(
//         () async {
//       WidgetsFlutterBinding.ensureInitialized();
//       await SpUtil.getInstance();
//       runApp(AppPage());
//
//       ///Android状态栏透明 splash为白色,所以调整状态栏文字为黑色
//       SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//         statusBarColor: Colors.transparent,
//       ));
//     },
//     onError: (Object obj, StackTrace stack) {
//       print(obj);
//       print(stack);
//     },
//   );
// }
///
///
/// 单个数据创建：
///
// @override
// List<SingleChildWidget> otherProviders(BuildContext context) => [
//   ///用户信息
//   VMBean.initCache<UserBean>(
//     UserBean.cacheKey,
//         (map) {
//       print('用户信息数据处理-->>$map');
//       return UserBean.fromJson(map);
//     },
//   ),
//
//   ///网络状态
//   VMBean.init<ConnectivityResult>(),
// ];
//
// @override
// bool get lazyViewModel => false;
///
///
/// 获取数据监听：
///
/// view类
// Consumer<VMBean<UserInfoBean>>(builder: (context, bean, _) => )
///
/// vm类
// var user = Provider.of<UserInfoBean>(context, listen: false);
///
///使用命令：flutter packages pub run build_runner build --delete-conflicting-outputs
class VMBean<T> with ChangeNotifier {
  ///缓存的key
  String cacheKey;

  ///运营的数据
  T vm;

  VMBean({
    this.cacheKey,
    this.vm,
  });

  ///封装下，数据提供者的快速获取方式
  ///listen手动默认为[false]
  static VMBean<T> provider<T>(BuildContext context, {bool listen = false}) {
    return Provider.of<VMBean<T>>(context, listen: listen ?? false);
  }

  ///封装下，数据提供者的快速获取方式
  static ChangeNotifierProvider<VMBean<T>> init<T>({
    Key key,
    Create<VMBean<T>> create,
    bool lazy,
    TransitionBuilder builder,
    Widget child,
  }) {
    return ChangeNotifierProvider<VMBean<T>>(
      key: key,
      create: create ?? (BuildContext context) => VMBean._empty(),
      lazy: lazy,
      builder: builder,
      child: child,
    );
  }

  ///封装下，从缓存读取数据提供者的快速获取方式
  static ChangeNotifierProvider<VMBean<T>> initCache<T>(
    String cacheKey,
    T f(Map<String, dynamic> map), {
    T defValue,
  }) {
    return VMBean.init<T>(
      create: (context) => _initCacheBean(
        cacheKey,
        f,
        defValue: defValue,
      ),
    );
  }

  ///初始化
  VMBean._empty();

  @override
  void dispose() {
    print('--vmBean调用${T.runtimeType}-->>dispose回收');
    super.dispose();
  }

  ///回收
  void disposeSelf() {}

  ///[init]
  ///[initCache]
  ///通用功能
  //////////////////////////////////////////////////////////////////////////////

  ///更新Future
  Future<VMBean<T>> updateSelfFuture(UpdateDataFuture<T> dataFuture) async {
    try {
      this.vm = await dataFuture?.call();
      notifyListeners();
    } catch (e) {
      print('更新Future-->$e');
    }
    return this;
  }

  ///更新
  VMBean<T> updateSelf(T vm) {
    try {
      this.vm = vm;
      notifyListeners();
    } catch (e) {
      print('更新失败-->$e');
    }
    return this;
  }

  ///清除内存数据
  void clearSelf() {
    try {
      if (this.vm != null) {
        this.vm = null;
        notifyListeners();
      }
    } catch (e) {
      print('清除内存数据-->$e');
    }
  }

  ///[initCache]
  ///特有功能
  //////////////////////////////////////////////////////////////////////////////

  ///封装下，从缓存读取数据，key关联[cacheKey]
  static VMBean<T> _initCacheBean<T>(
    String cacheKey,
    T f(Map<String, dynamic> map), {
    T defValue,
  }) {
    return VMBean._cacheInit(cacheKey)
      ..cacheToSelf(
        f,
        defValue: defValue,
      );
  }

  ///使用关联本地缓存，key关联[cacheKey]
  VMBean._cacheInit(this.cacheKey);

  ///读取缓存数据到vm，key关联[cacheKey]
  void cacheToSelf(T f(Map<String, dynamic> map), {T defValue}) async {
    if (this.cacheKey != null) {
      String _jsonString = SpUtil.getString(this.cacheKey, defValue: null);
      var _vm = defValue;
      if (_jsonString != null && _jsonString.isNotEmpty) {
        Map<String, dynamic> _jsonMap = jsonDecode(_jsonString);
        T _bean = f?.call(_jsonMap);
        if (_bean != null) {
          _vm = _bean;
        }
      }
      updateSelf(_vm);
    }
  }

  ///缓存vm的数据本地，key关联[cacheKey]
  Future<bool> cacheFromSelf(Map<String, dynamic> f(T vm)) async {
    if (this.cacheKey != null) {
      return await SpUtil.putObject(this.cacheKey, f(this.vm));
    }
    return false;
  }

  ///清理缓存数据
  Future<bool> cacheRemoveSelf() async {
    if (this.cacheKey != null) {
      return await SpUtil.remove(this.cacheKey);
    }
    return false;
  }

  ///清除内存数据
  ///清理缓存数据
  Future<bool> resetSelf() async {
    clearSelf();
    if (this.cacheKey != null) {
      return cacheRemoveSelf();
    }
    return true;
  }
}

///更新异步数据
typedef UpdateDataFuture<T> = Future<T> Function();
