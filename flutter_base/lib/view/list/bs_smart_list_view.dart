import 'package:flutter/material.dart';
import 'package:flutter_base/view/bs_view.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';

///数据逻辑
///使用模板查看[TestSmartListView]
///by grus95
class BsSmartListVM extends BsViewModel {
  ///布局类型
  final SmartType smartType;

  ///刷新加载控制器
  final EasyRefreshController refreshController;

  ///滚动控制器
  final ScrollController scrollController;

  ///新增数据List回调
  final OnAddSmartBeanListCallback smartBeanListCallback;

  ///默认值[BsSmartBean]
  final BsSmartBean dfSmartBean;

  BsSmartListVM({
    this.smartType,
    this.refreshController,
    this.scrollController,
    this.smartBeanListCallback,
    this.dfSmartBean,
  });

  ///数据结构
  final smartBeanVnr = ValueNotifier<BsSmartBean>(null);

  @override
  void buildCompleted(BuildContext context) {
    assert(_dfPageIndex > -1 && _dfPageSize > 0);
    _onFirstLoadingCallback(context);
  }

  @override
  void disposeVM() {
    refreshController?.dispose();
    scrollController?.dispose();
    smartBeanVnr.dispose();
  }

  @override
  void initVM(BuildContext context) {}

  ///默认下标
  int get _dfPageIndex => dfSmartBean?.dfPageIndex ?? BsSmartBean.bsPageIndex;

  ///默认条数
  int get _dfPageSize => dfSmartBean?.dfPageSize ?? BsSmartBean.bsPageSize;

  ///是否自动加载
  bool get _isAutoRefresh => dfSmartBean?.isAutoRefresh ?? true;

  ///首次加载数据动作
  Future<void> _onFirstLoadingCallback(BuildContext context) async {
    print('_smartBean--首次加载数据动作->>');
    smartBeanVnr.value = dfSmartBean..isFirstLoading = true;
    if (_isAutoRefresh) {
      await _onRefresh(
        context,
        smartBeanVnr: smartBeanVnr,
        smartBeanListCallback: smartBeanListCallback,
      );
    } else {
      _onUserInitData(context, smartBeanVnr: smartBeanVnr);
    }
  }

  ///使用默认数据
  void _onUserInitData(
    BuildContext context, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
  }) {
    var _beanList = smartBeanVnr?.value?.initData;
    _updateEnd(
      context,
      beanList: _beanList,
      smartBeanVnr: smartBeanVnr,
    );
  }

  ///空数据或者异常后，重新加载数据
  Future<void> onEmptyOrError2Refresh(BuildContext context) async {
    print('_smartBean--空数据或者异常后，重新加载数据->>');
    smartBeanVnr.value = BsSmartBean(
      dfPageIndex: _dfPageIndex,
      pageIndex: _dfPageIndex,
      dfPageSize: _dfPageSize,
      pageSize: _dfPageSize,
      isEmptyError2Loading: true,
    );
    await _onRefresh(
      context,
      smartBeanVnr: smartBeanVnr,
      smartBeanListCallback: smartBeanListCallback,
    );
  }

  ///下拉刷新动作
  onRefreshCallback(
    BuildContext context, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
    OnAddSmartBeanListCallback smartBeanListCallback,
  }) {
    BsSmartBean smartBean = smartBeanVnr.value;
    bool _hasRefresh = smartBeanListCallback != null &&
        ((smartBean?.isFirstLoading ?? false) ||
            (smartBean?.isRefreshEnable ?? false));
    print(
        '_smartBean--下拉刷新动作->>${smartBean?.data?.toString()}--smartBean-->>${smartBean == null}');

    ///强制能下拉刷新
    _hasRefresh = true;

    return _hasRefresh
        ? () async {
            await _onRefresh(
              context,
              smartBeanVnr: smartBeanVnr,
              smartBeanListCallback: smartBeanListCallback,
            );
          }
        : null;
  }

  Future<void> _onRefresh(
    BuildContext context, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
    OnAddSmartBeanListCallback smartBeanListCallback,
  }) async {
    smartBeanVnr.value = smartBeanVnr.value.clone()..pageIndex = _dfPageIndex;
    //开始
    _updateStart(context, smartBeanVnr: smartBeanVnr);
    //加载中
    _updateLoading(context, smartBeanVnr: smartBeanVnr);
    List _beanList = await smartBeanListCallback?.call(context, smartBeanVnr);
    //结束
    _updateEnd(
      context,
      beanList: _beanList,
      smartBeanVnr: smartBeanVnr,
    );
  }

  ///上拉加载更多动作
  onLoadingMoreCallback(
    BuildContext context, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
    OnAddSmartBeanListCallback smartBeanListCallback,
  }) {
    BsSmartBean smartBean = smartBeanVnr.value;
    bool _hasLoadMore = (smartBean?.isLoadingMoreEnable ?? false) &&
        (smartBean?.isHasNextPage ?? false) &&
        smartBeanListCallback != null;
    return _hasLoadMore
        ? () async {
            await _onLoadingMore(
              context,
              smartBeanVnr: smartBeanVnr,
              smartBeanListCallback: smartBeanListCallback,
            );
          }
        : null;
  }

  Future<void> _onLoadingMore(
    BuildContext context, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
    OnAddSmartBeanListCallback smartBeanListCallback,
  }) async {
    //开始
    _updateStart(context, smartBeanVnr: smartBeanVnr);
    //加载中
    _updateLoading(context, smartBeanVnr: smartBeanVnr);
    List _beanList = await smartBeanListCallback?.call(context, smartBeanVnr);
    //结束
    _updateEnd(
      context,
      beanList: _beanList,
      smartBeanVnr: smartBeanVnr,
    );
  }

  ///开始更新数据
  void _updateStart(
    BuildContext context, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
  }) {
    smartBeanVnr.value = smartBeanVnr.value.clone()
      ..smartRefreshLoadingState = SmartRefreshLoadingState.start;
    print('-开始更新数据-->>${smartBeanVnr.value?.toString()}');
  }

  ///正在更新数据
  void _updateLoading(
    BuildContext context, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
  }) {
    smartBeanVnr.value = smartBeanVnr.value.clone()
      ..smartFullViewState = SmartFullViewState.loading
      ..smartRefreshLoadingState = SmartRefreshLoadingState.loading;
    print('-正在更新数据-->>${smartBeanVnr.value?.toString()}');
  }

  ///结束更新数据
  void _updateEnd(
    BuildContext context, {
    List beanList,
    ValueNotifier<BsSmartBean> smartBeanVnr,
  }) {
    BsSmartBean smartBean = smartBeanVnr.value.clone();
    assert(smartBean != null);
    bool _suc = beanList != null;
    bool _hasNextPage = _suc && beanList.length >= _dfPageSize;
    bool _isRefresh = smartBean.pageIndex == _dfPageIndex;
    if (_suc) {
      smartBean?.pageIndex += 1;
    }
    //更新数据
    if (_isRefresh) {
      smartBean?.data = beanList;
    } else {
      if (_suc) {
        if (beanList?.isNotEmpty == true) {
          smartBean?.data?.addAll(beanList);
        }
      }
    }
    smartBean
      ..isFirstLoading = false
      ..isEmptyError2Loading = false
      ..isHasNextPage = _hasNextPage
      ..smartRefreshLoadingState = SmartRefreshLoadingState.end;

    ///修改状态
    smartBeanVnr.value = smartBean;

    if (_isRefresh) {
      refreshController?.finishRefresh(success: _suc);
    } else {
      refreshController?.finishLoad(success: _suc, noMore: !_hasNextPage);
    }
    var _smartFullViewState = SmartFullViewState.normal;
    if (!_suc) {
      _smartFullViewState = SmartFullViewState.error;
    } else {
      if (beanList.isEmpty) {
        _smartFullViewState = SmartFullViewState.empty;
      }
    }

    ///重置状态
    smartBeanVnr.value = smartBean.clone()
      ..smartFullViewState = _smartFullViewState
      ..smartRefreshLoadingState = SmartRefreshLoadingState.normal;

    print('-结束更新数据-->>${smartBeanVnr.value?.toString()}');
  }
}

///UI界面
///by grus95
abstract class BsSmartListView extends BsView<BsSmartListVM> {
  ///布局类型
  final SmartType smartType;

  /// Header
  final Header header;

  /// Footer
  final Footer footer;

  ///默认值[BsSmartBean]
  final BsSmartBean dfSmartBean;

  ///默认值为[true]
  ///是否显示首次全局刷新布局[firstRefreshWidget]
  ///是否显示全局异常布局[errorWidget]
  ///是否显示全局空数据布局[emptyWidget]
  final bool isShowFullState;

  ///第一次刷新布局
  final OnLoadingDataView firstRefreshWidget;

  ///网络错误布局
  final OnErrorDataView errorWidget;

  ///空数据布局
  final OnEmptyDataView emptyWidget;

  ///刷新加载控制器
  final EasyRefreshController refreshController;

  ///滚动控制器
  final ScrollController scrollController;

  /// 顶部回弹(Header的overScroll属性优先，且onRefresh和header都为null时生效)
  final bool topBouncing;

  /// 底部回弹(Footer的overScroll属性优先，且onLoad和footer都为null时生效)
  final bool bottomBouncing;

  ///新增数据List回调
  final OnAddSmartBeanListCallback smartBeanListCallback;

  ///数据绑定布局回调[SmartType.single]
  final OnSingle2View single2view;

  ///数据绑定布局回调[SmartType.item]
  final OnItem2View item2view;

  ///数据绑定布局回调[SmartType.slivers]
  final OnSlivers2View slivers2View;

  ///数据绑定布局回调[SmartType.builder]
  final OnBuilder2View builder2view;

  BsSmartListView({
    this.smartType,
    this.header,
    this.footer,
    this.dfSmartBean,
    this.isShowFullState,
    this.firstRefreshWidget,
    this.errorWidget,
    this.emptyWidget,
    this.refreshController,
    this.scrollController,
    this.topBouncing,
    this.bottomBouncing,
    this.smartBeanListCallback,
    this.single2view,
    this.item2view,
    this.slivers2View,
    this.builder2view,
  });

  @override
  Widget buildScaffold(BuildContext context) {
    return viewModelCS(
      (ct, vm, cd) => _pageBodyView(ct, vm),
    );
  }

  ///内容
  Widget _pageBodyView(BuildContext context, BsSmartListVM vm) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: ValueListenableBuilder<BsSmartBean>(
        valueListenable: vm.smartBeanVnr,
        builder: (context, smartBean, child) {
          //赋值后再处理
          bool _hasInit = smartBean != null;
          return _hasInit
              ? Stack(
                  children: [
                    _onListTypeView(context, vm),
                    _onFullStateView(context, vm),
                  ],
                )
              : Container();
        },
      ),
    );
  }

  ///全局状态布局
  Widget _onFullStateView(BuildContext context, BsSmartListVM vm) {
    ValueNotifier<BsSmartBean> smartBeanVnr = vm.smartBeanVnr;
    BsSmartBean smartBean = smartBeanVnr?.value;
    Widget _stateView = Container();
    print('-有更新布局->>全局状态布局-->${smartBean?.smartFullViewState}');
    bool _isShowFullState = isShowFullState ?? true;
    switch (smartBean?.smartFullViewState) {
      case SmartFullViewState.unknown:
        break;
      case SmartFullViewState.normal:
        break;
      case SmartFullViewState.loading:
        if (_isShowFullState &&
            (smartBean?.isFirstLoading == true ||
                smartBean?.isEmptyError2Loading == true)) {
          _stateView = _onRefreshWidget(context, vm);
        }
        break;
      case SmartFullViewState.empty:
        if (_isShowFullState && smartBean?.data?.isEmpty == true) {
          _stateView = _onEmptyDataView(context, vm);
        }
        break;
      case SmartFullViewState.error:
        if (_isShowFullState && smartBean?.data == null) {
          _stateView = _onErrorDataView(context, vm);
        }
        break;
      default:
        break;
    }

    return _stateView;
  }

  ///列表类型数据布局
  Widget _onListTypeView(BuildContext context, BsSmartListVM vm) {
    ValueNotifier<BsSmartBean> _smartBeanVnr = vm.smartBeanVnr;
    Widget _typeView = Container();
    switch (vm.smartType) {
      case SmartType.single:
        _typeView = _singleStateView(
          context,
          vm,
          smartBeanVnr: _smartBeanVnr,
        );
        break;
      case SmartType.item:
        _typeView = _itemStateView(
          context,
          vm,
          smartBeanVnr: _smartBeanVnr,
        );
        break;
      case SmartType.slivers:
        _typeView = _sliversStateView(
          context,
          vm,
          smartBeanVnr: _smartBeanVnr,
        );
        break;
      case SmartType.builder:
        _typeView = _builderStateView(
          context,
          vm,
          smartBeanVnr: _smartBeanVnr,
        );
        break;
      default:
        break;
    }
    return _typeView;
  }

  ///[single]类型布局
  Widget _singleStateView(
    BuildContext context,
    BsSmartListVM vm, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
  }) {
    return EasyRefresh(
      enableControlFinishRefresh: true,
      enableControlFinishLoad: true,
      taskIndependence: false,
      controller: vm.refreshController,
      scrollController: vm.scrollController,
      topBouncing: topBouncing ?? true,
      bottomBouncing: bottomBouncing ?? true,
      firstRefresh: false,
      firstRefreshWidget: null,
      emptyWidget: null,
      header: _header(context),
      footer: _footer(context),
      onRefresh: vm.onRefreshCallback(
        context,
        smartBeanVnr: smartBeanVnr,
        smartBeanListCallback: smartBeanListCallback,
      ),
      onLoad: vm.onLoadingMoreCallback(
        context,
        smartBeanVnr: smartBeanVnr,
        smartBeanListCallback: smartBeanListCallback,
      ),
      child: single2view?.call(context, smartBeanVnr) ?? Container(),
    );
  }

  ///[item]类型布局
  Widget _itemStateView(
    BuildContext context,
    BsSmartListVM vm, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
  }) {
    return EasyRefresh.custom(
      enableControlFinishRefresh: true,
      enableControlFinishLoad: true,
      taskIndependence: false,
      controller: vm.refreshController,
      scrollController: vm.scrollController,
      reverse: false,
      scrollDirection: Axis.vertical,
      topBouncing: topBouncing ?? true,
      bottomBouncing: bottomBouncing ?? true,
      firstRefresh: false,
      firstRefreshWidget: null,
      emptyWidget: null,
      header: _header(context),
      footer: _footer(context),
      onRefresh: vm.onRefreshCallback(
        context,
        smartBeanVnr: smartBeanVnr,
        smartBeanListCallback: smartBeanListCallback,
      ),
      onLoad: vm.onLoadingMoreCallback(
        context,
        smartBeanVnr: smartBeanVnr,
        smartBeanListCallback: smartBeanListCallback,
      ),
      slivers: [
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              var _dataList = smartBeanVnr?.value?.data;
              var _bean = _dataList != null && index < _dataList.length
                  ? _dataList[index]
                  : null;
              return item2view?.call(context, smartBeanVnr, _bean, index) ??
                  Container();
            },
            childCount: smartBeanVnr?.value?.data?.length ?? 0,
          ),
        ),
      ],
    );
  }

  ///[slivers]类型布局
  Widget _sliversStateView(
    BuildContext context,
    BsSmartListVM vm, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
  }) {
    return EasyRefresh.custom(
      enableControlFinishRefresh: true,
      enableControlFinishLoad: true,
      taskIndependence: false,
      controller: vm.refreshController,
      scrollController: vm.scrollController,
      reverse: false,
      scrollDirection: Axis.vertical,
      topBouncing: topBouncing ?? true,
      bottomBouncing: bottomBouncing ?? true,
      firstRefresh: false,
      firstRefreshWidget: null,
      emptyWidget: null,
      header: _header(context),
      footer: _footer(context),
      onRefresh: vm.onRefreshCallback(
        context,
        smartBeanVnr: smartBeanVnr,
        smartBeanListCallback: smartBeanListCallback,
      ),
      onLoad: vm.onLoadingMoreCallback(
        context,
        smartBeanVnr: smartBeanVnr,
        smartBeanListCallback: smartBeanListCallback,
      ),
      slivers: slivers2View?.call(context, smartBeanVnr) ?? [],
    );
  }

  ///[builder]类型布局
  /// 用法灵活,但需将physics、header和footer放入列表中
  Widget _builderStateView(
    BuildContext context,
    BsSmartListVM vm, {
    ValueNotifier<BsSmartBean> smartBeanVnr,
  }) {
    return EasyRefresh.builder(
      enableControlFinishRefresh: true,
      enableControlFinishLoad: true,
      taskIndependence: false,
      controller: vm.refreshController,
      scrollController: vm.scrollController,
      topBouncing: topBouncing ?? true,
      bottomBouncing: bottomBouncing ?? true,
      firstRefresh: false,
      header: _header(context),
      footer: _footer(context),
      onRefresh: vm.onRefreshCallback(
        context,
        smartBeanVnr: smartBeanVnr,
        smartBeanListCallback: smartBeanListCallback,
      ),
      onLoad: vm.onLoadingMoreCallback(
        context,
        smartBeanVnr: smartBeanVnr,
        smartBeanListCallback: smartBeanListCallback,
      ),
      builder: (
        BuildContext context,
        ScrollPhysics physics,
        Widget header,
        Widget footer,
      ) {
        return builder2view?.call(
              context,
              smartBeanVnr,
              physics,
              header,
              footer,
            ) ??
            Container();
      },
    );
  }

  ///第一次加载布局
  Widget _onRefreshWidget(BuildContext context, BsSmartListVM vm) =>
      firstRefreshWidget?.call(context, vm) ?? dfLoadingDataView(context, vm);

  ///正在加载数据的时候，显示的界面
  Widget dfLoadingDataView(BuildContext context, BsSmartListVM vm);

  ///没有数据的时候，显示的界面
  Widget _onEmptyDataView(BuildContext context, BsSmartListVM vm) {
    return emptyWidget?.call(context, vm) ?? dfEmptyDataView(context, vm);
  }

  ///没有数据的时候，显示的界面
  Widget dfEmptyDataView(BuildContext context, BsSmartListVM vm);

  ///错误数据的时候，显示的界面
  Widget _onErrorDataView(BuildContext context, BsSmartListVM vm) {
    return errorWidget?.call(context, vm) ?? dfErrorDataView(context, vm);
  }

  ///错误数据的时候，显示的界面
  Widget dfErrorDataView(BuildContext context, BsSmartListVM vm);

  ///Header
  Header _header(BuildContext context) => header ?? dfHeader(context);

  Header dfHeader(BuildContext context);

  ///Footer
  Footer _footer(BuildContext context) => footer ?? dfFooter(context);

  Footer dfFooter(BuildContext context);

  ///配置默认列表下标
  int _dfPageListIndex(BuildContext context) => dfPageListIndex(context);

  ///配置默认列表下标
  int dfPageListIndex(BuildContext context);

  ///配置默认列表条数
  int _dfPageListSize(BuildContext context) => dfPageListSize(context);

  ///配置默认列表条数
  int dfPageListSize(BuildContext context);

  @override
  BsSmartListVM buildViewModel(BuildContext context) {
    return BsSmartListVM(
      smartType: smartType,
      refreshController: refreshController ?? EasyRefreshController(),
      scrollController: scrollController ?? ScrollController(),
      smartBeanListCallback: smartBeanListCallback,
      dfSmartBean: (dfSmartBean ?? BsSmartBean())
        ..dfPageIndex = _dfPageListIndex(context)
        ..pageIndex = _dfPageListIndex(context)
        ..dfPageSize = _dfPageListSize(context)
        ..pageSize = _dfPageListSize(context),
    );
  }
}

///[SmartType.single]类型，获取数据成功后回调
typedef OnSingle2View<T> = Widget Function(
  BuildContext context,
  ValueNotifier<BsSmartBean<T>> smartBeanVnr,
);

///[SmartType.item]类型，获取数据成功后回调
typedef OnItem2View<T> = Widget Function(
  BuildContext context,
  ValueNotifier<BsSmartBean<T>> smartBeanVnr,
  T bean,
  int index,
);

///[SmartType.slivers]类型，获取数据成功后回调
typedef OnSlivers2View<T> = List<Widget> Function(
  BuildContext context,
  ValueNotifier<BsSmartBean<T>> smartBeanVnr,
);

///[SmartType.builder]类型，获取数据成功后回调
typedef OnBuilder2View<T> = Widget Function(
  BuildContext context,
  ValueNotifier<BsSmartBean<T>> smartBeanVnr,
  ScrollPhysics physics,
  Widget header,
  Widget footer,
);

///正在加载数据的时候，显示的界面
typedef OnLoadingDataView = Widget Function(
  BuildContext context,
  BsSmartListVM vm,
);

///没有数据的时候，显示的界面
typedef OnEmptyDataView = Widget Function(
  BuildContext context,
  BsSmartListVM vm,
);

///错误数据的时候，显示的界面
typedef OnErrorDataView = Widget Function(
  BuildContext context,
  BsSmartListVM vm,
);

///更新beanList数据
///添加的数据会保存在[BsSmartBean.data]中
typedef OnAddSmartBeanListCallback = Future<List> Function(
  BuildContext context,
  ValueNotifier<BsSmartBean> smartBeanVnr,
);

///类型值
enum SmartType {
  ///单布局类型
  single,

  ///多布局类型
  item,

  ///布局自定义类型
  slivers,

  ///自定义构造器类型
  builder,
}

///全局界面状态state
enum SmartFullViewState {
  ///未知
  unknown,

  ///正常显示数据列表
  normal,

  ///加载数据中
  loading,

  ///数据长度为0
  empty,

  ///异常
  error,
}

///数据加载状态state
enum SmartRefreshLoadingState {
  ///未知
  unknown,

  ///正常
  normal,

  ///开始
  start,

  ///加载中
  loading,

  ///结束
  end,
}

///内部数据构造
class BsSmartBean<T> {
  ///时间
  int millisecond;

  ///全局界面状态state
  SmartFullViewState smartFullViewState;

  ///数据加载状态state
  SmartRefreshLoadingState smartRefreshLoadingState;

  ///提示msg
  String msg;

  ///数据
  List<T> data;

  ///初始化数据
  List<T> initData;

  ///当前页数-默认
  int dfPageIndex;

  ///当前页数
  int pageIndex;

  ///每次条数-默认
  int dfPageSize;

  ///每次条数
  int pageSize;

  ///自动刷新数据
  bool isAutoRefresh;

  ///是否第一次加载
  bool isFirstLoading;

  ///空数据或者异常后重新加载
  bool isEmptyError2Loading;

  ///是否开启刷新功能
  bool isRefreshEnable;

  ///是否开启加载更多功能
  bool isLoadingMoreEnable;

  ///是否有下一页
  bool isHasNextPage;

  BsSmartBean({
    this.millisecond = 0,
    this.smartFullViewState = SmartFullViewState.unknown,
    this.smartRefreshLoadingState = SmartRefreshLoadingState.unknown,
    this.msg,
    this.data,
    this.initData,
    this.dfPageIndex = bsPageIndex,
    this.pageIndex = bsPageIndex,
    this.dfPageSize = bsPageSize,
    this.pageSize = bsPageSize,
    this.isAutoRefresh = true,
    this.isFirstLoading = true,
    this.isEmptyError2Loading = true,
    this.isRefreshEnable = true,
    this.isLoadingMoreEnable = true,
    this.isHasNextPage = true,
  });

  ///默认列表下标值
  static const int bsPageIndex = 0;

  ///默认列表每次条数值
  static const int bsPageSize = 20;

  ///查询获取指定[index]的数据
  T queryItemData(int index) {
    var _data = data;
    bool _rightIndex = index != null && index > -1 && index < _data.length;
    assert(_rightIndex);
    if (_data?.isNotEmpty == true && _rightIndex) {
      return _data[index];
    }
    return null;
  }

  ///是否为首页数据
  bool isFirstPage() {
    return pageIndex == dfPageIndex;
  }

  ///是否为第一条数据
  bool isFirstItem(int index) {
    return index == 0;
  }

  ///是否为最后一条数据
  bool isLastItem(int index) {
    return index == data.length - 1;
  }

  ///是否能显示没有更多
  bool showNoMoreViewEnable(int index) {
    assert(index >= 0);
    return isHasNextPage != true && isLastItem(index);
  }

  ///克隆新的数据对象
  BsSmartBean<T> clone([bean]) {
    return BsSmartBean<T>(
      millisecond: DateTime.now().millisecondsSinceEpoch,
      smartFullViewState: bean?.smartFullViewState ?? this.smartFullViewState,
      smartRefreshLoadingState:
          bean?.smartRefreshLoadingState ?? this.smartRefreshLoadingState,
      msg: bean?.msg ?? this.msg,
      data: bean?.data ?? this.data,
      initData: bean?.initData ?? this.initData,
      dfPageIndex: bean?.dfPageIndex ?? this.dfPageIndex,
      pageIndex: bean?.pageIndex ?? this.pageIndex,
      dfPageSize: bean?.dfPageSize ?? this.dfPageSize,
      pageSize: bean?.pageSize ?? this.pageSize,
      isAutoRefresh: bean?.isAutoRefresh ?? this.isAutoRefresh,
      isFirstLoading: bean?.isFirstLoading ?? this.isFirstLoading,
      isEmptyError2Loading:
          bean?.isEmptyError2Loading ?? this.isEmptyError2Loading,
      isRefreshEnable: bean?.isRefreshEnable ?? this.isRefreshEnable,
      isLoadingMoreEnable:
          bean?.isLoadingMoreEnable ?? this.isLoadingMoreEnable,
      isHasNextPage: bean?.isHasNextPage ?? this.isHasNextPage,
    );
  }

  @override
  String toString() {
    return 'millisecond=>$millisecond\n' +
        'smartFullViewState=>$smartFullViewState\n' +
        'smartRefreshLoadingState=>$smartRefreshLoadingState\n' +
        'msg=>$msg\n' +
        'data=>$data\n' +
        'initData=>$initData\n' +
        'dfPageIndex=>$dfPageIndex\n' +
        'pageIndex=>$pageIndex\n' +
        'dfPageSize=>$dfPageSize\n' +
        'pageSize=>$pageSize\n' +
        'isAutoRefresh=>$isAutoRefresh\n' +
        'isFirstLoading=>$isFirstLoading\n' +
        'isEmptyError2Loading=>$isEmptyError2Loading\n' +
        'isRefreshEnable=>$isRefreshEnable\n' +
        'isLoadingMoreEnable=>$isLoadingMoreEnable\n' +
        'isHasNextPage=>$isHasNextPage\n';
  }
}
