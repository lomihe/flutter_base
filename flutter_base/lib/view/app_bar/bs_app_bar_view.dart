import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/view/app_bar/auto_app_bar_item_view.dart';

///导航栏
///使用模板查看[TestAppBarView]
///by grus95
abstract class BsAppBarView extends StatelessWidget
    implements PreferredSizeWidget {
  ///高度
  final double appBarHeight;

  ///左边布局
  final Widget leadingView;

  ///左边布局-宽度
  final double leadingWidth;

  ///返回按钮布局
  final Widget backView;

  ///返回按钮布局-点击
  final GestureTapCallback backOnTap;

  ///返回按钮布局-长按
  final GestureLongPressCallback backOnLongPress;

  ///返回按钮布局-是否显示
  final bool backShowEnable;

  ///返回按钮布局-颜色
  final Color backColor;

  ///标题布局
  final Widget titleView;

  ///标题布局-名称
  final String titleName;

  ///标题布局-颜色
  final Color titleColor;

  ///标题布局-大小
  final double titleFontSize;

  ///是否居中标题
  final bool centerTitle;

  final Color backgroundColor;

  final Brightness brightness;

  final PreferredSizeWidget bottom;

  final List<Widget> actions;

  BsAppBarView({
    this.appBarHeight,
    this.leadingView,
    this.leadingWidth,
    this.backView,
    this.backOnTap,
    this.backOnLongPress,
    this.backColor,
    this.backShowEnable,
    this.titleView,
    this.titleName,
    this.titleColor,
    this.titleFontSize,
    this.centerTitle,
    this.backgroundColor,
    this.brightness,
    this.bottom,
    this.actions,
  });

  @override
  Size get preferredSize => Size.fromHeight(_dfAppBarHeight);

  ///默认appbar高度
  double get _dfAppBarHeight {
    return ((appBarHeight ?? dfAppBarHeight()) ?? 44) +
        (bottom?.preferredSize?.height ?? 0.0);
  }

  double dfAppBarHeight();

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: _dfLeadingView(context),
      leadingWidth: leadingWidth,
      title: _dfTitleView(context),
      centerTitle: centerTitle,
      actions: actions,
      backgroundColor: _dfBackgroundColor(context),
      brightness: _dfBrightness(context),
      elevation: 0,
      toolbarHeight: _dfAppBarHeight,
      bottom: bottom,
    );
  }

  ///默认背景颜色
  Color _dfBackgroundColor(BuildContext context) {
    return backgroundColor ?? dfBackgroundColor(context);
  }

  Color dfBackgroundColor(BuildContext context);

  ///默认Brightness
  Brightness _dfBrightness(BuildContext context) {
    return brightness ?? dfBrightness(context) ?? Brightness.dark;
  }

  Brightness dfBrightness(BuildContext context);

  ///默认返回按钮
  Widget _dfBackView(BuildContext context) {
    return backView ?? dfBackView(context, _dfBrightness(context));
  }

  Widget dfBackView(BuildContext context, Brightness brightness);

  ///默认左边布局
  Widget _dfLeadingView(BuildContext context) {
    return leadingView ?? dfLeadingView(context);
  }

  Widget dfLeadingView(BuildContext context) {
    //是否能返回
    bool _canPop = Navigator.of(context).canPop();
    bool _isShowBack = (backShowEnable ?? _canPop) ?? false;
    Widget _dfBack = _dfBackView(context);
    return Visibility(
      visible: _isShowBack,
      child: GestureDetector(
        onTap: backOnTap ??
            () {
              if (_canPop) {
                Navigator.of(context).pop();
              }
            },
        onLongPress: backOnLongPress,
        child: Container(
          color: Colors.transparent,
          width: appBarHeight,
          height: appBarHeight,
          alignment: Alignment.center,
          child: _dfBack,
        ),
      ),
    );
  }

  ///默认标题布局
  Widget _dfTitleView(BuildContext context) {
    return titleView ??
        dfTitleView(
          context,
          titleName: titleName ?? '',
          titleColor: titleColor,
          titleFontSize: titleFontSize,
        );
  }

  Widget dfTitleView(
    BuildContext context, {
    String titleName,
    Color titleColor,
    double titleFontSize,
  });
}

///导航栏中间布局
class AutoAppBarLeadingView extends StatelessWidget {
  ///最左边布局
  final Widget leadingItem;

  ///返回按钮颜色
  final Color backColor;

  ///最左边布局-点击
  final GestureTapCallback leadingOnTap;

  ///最左边布局-是否显示
  final bool isShowLeading;

  ///背景类型
  final Brightness brightness;

  const AutoAppBarLeadingView(
      {Key key,
      this.leadingItem,
      this.backColor,
      this.leadingOnTap,
      this.brightness,
      this.isShowLeading})
      : super(key: key);

  Widget get backLightView => Image.asset(
        'static/webp/ic_back_light.webp',
      );

  Widget get backDarkView => Image.asset(
        'static/webp/ic_back_dark.webp',
      );

  // Widget get backSvgView => SvgPicture.asset(
  //       'static/svg/ic_back.svg',
  //       width: 22,
  //       height: 22,
  //       color: backSvgColor,
  //     );
  //
  // Color get backSvgColor =>
  //     backColor ??
  //     (brightness == Brightness.dark
  //         ? const Color(0xFFFFFEFE)
  //         : const Color(0xFF202020));

  @override
  Widget build(BuildContext context) {
    bool _canPop = Navigator.of(context).canPop();
    Widget _dfBack = leadingItem ??
        (brightness == Brightness.dark ? backLightView : backDarkView);
    Widget leading = AutoAppBarItemView(
      item: _dfBack,
      itemOnTap: leadingOnTap ??
          () {
            if (_canPop) {
              Navigator.of(context).pop();
            }
          },
      isShowItem: isShowLeading ?? _canPop,
    );
    return leading;
  }
}

///导航栏中间布局
class AutoAppBarTitleView extends StatelessWidget {
  final String titleName;
  final double fontSizeAdapter;
  final Color color;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final TextStyle style;

  const AutoAppBarTitleView({
    Key key,
    this.titleName,
    this.fontSizeAdapter,
    this.color,
    this.fontWeight,
    this.textAlign,
    this.style,
  }) : super(key: key);

  double get fontSize => fontSizeAdapter ?? 16;

  @override
  Widget build(BuildContext context) {
    return Text(
      titleName ?? '',
      textAlign: textAlign ?? TextAlign.center,
      style: style ??
          TextStyle(
            fontSize: fontSize,
            color: color ?? Color(0xFFFFFEFE),
            fontWeight: fontWeight ?? FontWeight.normal,
          ),
    );
  }
}
