import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/view/app_bar/bs_app_bar_view.dart';

class TestAppBarView extends BsAppBarView {
  ///高度
  final double appBarHeight;

  ///左边布局
  final Widget leadingView;

  ///左边布局-宽度
  final double leadingWidth;

  ///返回按钮布局
  final Widget backView;

  ///返回按钮布局-点击
  final GestureTapCallback backOnTap;

  ///返回按钮布局-长按
  final GestureLongPressCallback backOnLongPress;

  ///返回按钮布局-是否显示
  final bool backShowEnable;

  ///返回按钮布局-颜色
  final Color backColor;

  ///标题布局
  final Widget titleView;

  ///标题布局-名称
  final String titleName;

  ///标题布局-颜色
  final Color titleColor;

  ///标题布局-大小
  final double titleFontSize;

  ///是否居中标题
  final bool centerTitle;

  final Color backgroundColor;

  final Brightness brightness;

  final PreferredSizeWidget bottom;

  final List<Widget> actions;

  TestAppBarView({
    this.appBarHeight,
    this.leadingView,
    this.leadingWidth,
    this.backView,
    this.backOnTap,
    this.backOnLongPress,
    this.backColor,
    this.backShowEnable,
    this.titleView,
    this.titleName,
    this.titleColor,
    this.titleFontSize,
    this.centerTitle,
    this.backgroundColor,
    this.brightness,
    this.bottom,
    this.actions,
  });

  @override
  double dfAppBarHeight() {
    return 44;
  }

  @override
  Widget dfBackView(BuildContext context, Brightness brightness) {
    Widget _backView = Icon(
      CupertinoIcons.back,
      color: brightness == Brightness.dark ? Colors.white : Colors.black,
      size: 23,
    );
    return Container(
      child: _backView,
    );
  }

  @override
  Color dfBackgroundColor(BuildContext context) {
    return Colors.black;
  }

  @override
  Brightness dfBrightness(BuildContext context) {
    return Brightness.dark;
  }

  @override
  Widget dfTitleView(
    BuildContext context, {
    String titleName,
    Color titleColor,
    double titleFontSize,
  }) {
    return Text(
      titleName ?? '',
      textAlign: TextAlign.center,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontSize: titleFontSize,
        color: titleColor,
        fontWeight: FontWeight.normal,
      ),
    );
  }
}
